# Dépeendences

## Usage
positional arguments:
  print

optional arguments:
  -h, --help            show this help message and exit
  -t, --text
  -c, --cut
  -f, --fullcut
  -o, --old
  --font FONT
  --font-size FONT_SIZE
  --marge MARGE
  --center
  --canvas CANVAS
  --prefix PREFIX
  --dry-run
  --background BACKGROUND
  

## convert PDF to JPG
yaourt -S ghostscript
