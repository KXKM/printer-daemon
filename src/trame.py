# -*- coding: utf-8 -*-

import os
import unicodedata
import string
import hashlib
import textwrap

from PIL import Image, ImageDraw, ImageFont, ImageOps


valid_filename_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)


def clean_filename(filename, whitelist=valid_filename_chars, replace=' ', char_limit=255, with_hash=False):
    # replace spaces
    for r in replace:
        filename = filename.replace(r, '_')

    # keep only valid ascii chars
    cleaned_filename = unicodedata.normalize('NFKD', filename).encode('ASCII',
                                                                      'ignore').decode()

    if with_hash is not False:
        char_limit -= with_hash
        hash = hashlib.md5()
        hash.update(filename.encode())
        hash = hash.hexdigest()[:with_hash]
    else:
        hash = ''

    # keep only whitelisted chars
    cleaned_filename = ''.join(c for c in cleaned_filename if c in whitelist)
    if len(cleaned_filename) > char_limit:
        print("Warning, filename truncated because it was over {}. "
              "Filenames may no longer be unique".format(char_limit))
    return cleaned_filename[:char_limit] + hash


def draw_background(canva, t_size, background):
    back = Image.open(os.path.join(os.path.dirname(__file__), '../assets', background))
    b_size = back.size
    n = int(t_size[0] / b_size[0]) + 1
    for i in range(0, n):
        # print("i = {} / p_x = {}".format(i, i*b_size[0]))
        canva.paste(back, (i * b_size[0], 0))


def generate_trame(text, fontname, background, font_size='100%', fixe_size=None, center=True, marge=False, offset="0%"):

    max_height = 567
    interline_height = 0
    
    lines = text.split('\n')
    n_lines = len(lines)
    
    target_line_height = int(max_height/n_lines)
    
    font_at100 = ImageFont.truetype( os.path.join(os.path.dirname(__file__), '/data/fonts', fontname), 100)
    height_at100 = font_at100.getsize("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZëµ@`§")[1]
    fontsize_corrector = 100/height_at100
    if fontsize_corrector > 1: fontsize_corrector = 1
    print("fontsize_corrector = {}".format(fontsize_corrector))
    
    # calculate relative size for font (based on % of full height)
    if font_size.endswith('%'):
        font_size = int( max_height * int(font_size[:-1]) / 100 )
        
    # reduce fontsize for each lines
    font_size = int( (font_size - interline_height*(n_lines-1)) / n_lines ) 
    
    # apply fontsize_corrector
    font_size = int(font_size * fontsize_corrector)

    # FONT
    font = ImageFont.truetype( os.path.join(os.path.dirname(__file__), '/data/fonts', fontname), font_size)

    # SIZES
    total_size = [0,0]
    lines_size = [[] for t in text]
    
    # find lines and total sizes (w,h)
    for i,t in enumerate(lines):
        lines_size[i] = list(font.getsize(t))
        total_size[0] = max(total_size[0], lines_size[i][0])
        total_size[1] += target_line_height #lines_size[i][1]
        print("line", i, 'height', lines_size[i][1], 'target height', target_line_height)
        
    print("textarea size = {}".format(total_size))
    
    # canvas
    canva = Image.new('L', (total_size[0] if fixe_size is None else fixe_size[0], max_height), color=200)
    draw = ImageDraw.Draw(canva)
    draw_background(canva, total_size, background)
    
    if offset.endswith('%'):
        offset = int( max_height * int(offset[:-1]) / 100 )
    
    # draw
    # h_offset = (567-total_size[1])/2
    start_y = 0
    if str(fontname).startswith('Utopia'):
        start_y += 20
        
    for i,t in enumerate(lines):
        # X offset
        if center is True:
            start_x = (total_size[0] - lines_size[i][0]) / 2
        elif marge is not False:
            start_x = int(marge)
        else:
            start_x = 0
            
        start_y += int(offset)

        draw.text( (start_x, start_y + target_line_height*i), t, font=font, fill=0)
        # y_offset += interline_height + int(lines_size[i][1] if fixe_size is None else int(fixe_size[1]/len(text)) )

    # canva = ImageOps.expand(canva, border=1,fill='black') # Border
    return canva.rotate(270, expand=1)
    


def generate_multiple_trames(txt, font_size, font_path, background, canvas,
                             center=False, marge=False, offset="0%"):
    font = ImageFont.truetype( os.path.join(os.path.dirname(__file__), '/data/fonts', font_path), font_size)
    t_size = list(font.getsize(txt))
    txt_len = len(txt)
    if marge is not False:
        canvas -= int(marge)

    avg_char_size = float(t_size[0])/txt_len
    avg_char_per_canvas = int(canvas/avg_char_size)
    start = 0
    files = list()

    wrapper = textwrap.TextWrapper(width=avg_char_per_canvas)

    while start < txt_len:
        print("start : {} / {}; width = {}".format(start, txt_len, wrapper.width))
        try:
            txt_wrap = wrapper.wrap(txt[start:])[0]
        except IndexError as e:
            print("WARNING : txt_wrap indexerror : {}".format(e))
            break
        print(txt_wrap)
        # end = start + avg_char_per_canvas
        line_size = font.getsize(txt_wrap)
        while line_size[0] > canvas:
            wrapper.width -= 1
            txt_wrap = wrapper.wrap(txt[start:])[0]
            if wrapper.width > 0:
                line_size = font.getsize(txt_wrap)
            else:
                raise ValueError("Taille de canvas trop petite pour afficher un seul caractère")
        wrapper.width = avg_char_per_canvas # reset
        print("{}".format(txt_wrap))
        trame =  generate_trame(txt_wrap.strip(), font_size, font_path, background,
                                    fixe_size=[canvas, t_size[1]],
                                    center=center, marge=marge, offset=offset)
        if trame:
            files.append(trame)
            start += len(txt_wrap)

    return files




